#!/bin/bash
set -e
## collecting the AMI's from AWS
function collecting_ami(){
    aws ec2 describe-images --region $region --filters "Name=tag:$tag,Values=$value" --query 'Images[*].[ImageId,CreationDate]' --output text > ami.txt
}
## calculating the date
function ami_collection(){
    current_date=$(date +%Y-%m-%d)
    retention_date="$(date -d "$current_date - $days days" +%Y-%m-%d)"
    ami=0
    snapshots=0
    latest=0
    for ami_dates in `cat ami.txt | awk '{print $2}' | awk -FT '{print $1}' | sort | uniq`
    do
        if [[ "$ami_dates" > "$retention_date" ]];
        then
            latest=$((latest+1))
        else
            grep $ami_dates ami.txt | awk '{print $1}' >> old_ami.txt
            grep $ami_dates ami.txt | awk '{print $0}' | awk -FT '{print $1}' >> old_ami_dates.txt
        fi
    done
}
## If no latest AMI then halt the last old AMI to delete
function last_ami(){
    sed '$d' old_ami_dates.txt > temp.txt
    cat temp.txt | awk '{print $1}' > old_ami.txt
    rm -rf temp.txt
}
## Deleting the AMI's and Snapshots
function deletion(){
    if [[ -s old_ami.txt ]];
    then
        for ami_id in `cat old_ami.txt`
        do
                echo $ami_id "-----Deleted"
                ami=$((ami+1))
                ## aws ec2 deregister-image --image-id $ami_id
        done
        echo ""
        echo "--------------------------------------------"
        echo $ami "AMI's are deleted from your aws account"
        echo "--------------------------------------------"
        echo ""
        aws ec2 describe-images --image-ids `cat old_ami.txt` | grep snap | cut -d ':' -f 2 | cut -d '"' -f 2 > snap.txt
        for snap_id in `cat snap.txt`
        do
                echo $snap_id "-----Deleted"
                snapshots=$((snapshots+1))
                ## aws ec2 delete-snapshot --snapshot-id $snap_id
        done
        echo ""
        echo "-----------------------------------------------------------------"
        echo $snapshots "Respective snapshots are deleted from your aws account"
        echo "-----------------------------------------------------------------"
        
    else
        echo "There is no AMI older then $days days"
    fi
}
## Deleting the files which have created
function delete_files(){
    rm -rf ami.txt old_ami.txt old_ami_dates.txt snap.txt
    # sleep 5
    # clear
}
## Asking for input to proceed or deny
function input(){
    if [[ $REPLY =~ ^[Yy]$ ]];
    then
        deletion
    elif [[ $REPLY =~ ^[Nn]$ ]];
    then
        echo "As you have selected No, we are not moving ahead"
    
    else
        echo "Please enter the valid option"
    fi
}
region=$1
tag=$2
value=$3
days=$4
region_name=$(aws lightsail get-regions --query "regions[?name=='$region'].displayName" --output text)
collecting_ami
if [[ -s ami.txt ]];
then
    ami_collection
    latest_date=$(cat ami.txt  | sort -k 2 | sed '$!d' | awk -FT '{print $1}' | awk '{print $2}')
    time=$(cat ami.txt  | sort -k 2 | sed '$!d' | awk '{print $2}' | awk -FT '{print $2}' | cut -d '.' -f 1)
    echo $latest
    echo ""
    echo "---------------------------------------------------------------"
    echo "Your last build was on" $latest_date "at" $time in $region_name
    echo "---------------------------------------------------------------"
    if [[ $latest == 0 ]]; 
    then
        last_ami
    fi
    Total=`cat ami.txt | wc -l`
    old_ami_total=`cat old_ami.txt | wc -l`
    safe_ami=$((Total - old_ami_total))
    echo ""
    echo "-----------------------------------------------"
    if [[ $safe_ami == 0 ]]; 
    then
        echo "There is no AMI which is latest in your AWS"
    elif [[ $safe_ami == 1 ]]; 
    then
        echo "Single AMI is there which is latest in your AWS"
    else
        echo $safe_ami "AMI's are latest in your AWS"
    fi
    echo "-----------------------------------------------"
    echo ""
    echo "---------------------------------------------------------------------"
    if [[ $old_ami_total == 0 ]]; 
    then
        echo "No AMI's are there which are $days days older"
    elif [[ $old_ami_total == 1 ]]; 
    then
        echo "Only single AMI is there which is $days days older is stored in deletion file"
    else
        echo $old_ami_total "AMI's are there which are $days days older stored in deletion file"
    fi
    echo "---------------------------------------------------------------------"
    echo ""
    read -p "Are you sure that you want to delete? (y/n) " -n 1 -r
    echo ""
    echo ""
    
    input
else
    echo "---------------------------------------------------------------------------------------"
    echo "No AMI's are there with this tag -----> $tag":" $value in $region ($region_name) region"
    echo "---------------------------------------------------------------------------------------"
fi
delete_files